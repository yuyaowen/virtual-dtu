#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
#if (QT_VERSION > QT_VERSION_CHECK(5,6,0))
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

//    QFile skin(":/skin.qss");
//    skin.open(QIODevice::ReadOnly);
//    QString configs = skin.readAll();
//    a.setStyleSheet(configs);

    return a.exec();
}
