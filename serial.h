#ifndef SERIAL_H
#define SERIAL_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QtSerialPort>
#include <QSerialPort>
#include <QByteArray>

class Serial : public QObject
{
    Q_OBJECT

public:
    Serial();
    ~Serial();

    void portOpen(void);
    void portClose(void);
    void changeTo(QString portName);

    QStringList serialPortList;
    bool isHexDisplay = true;
    bool isHexSend = true;
    bool isSerialOpened = false;
    int baudRate = 115200;
    int dataBit = 8;
    int checkBit = 0;
    int stopBit = 1;

    QByteArray comData;
    QSerialPort *comPort = Q_NULLPTR;

private:


signals:
    void readDone();

private slots:
    void readSerial(void);
};

#endif // SERIAL_H
