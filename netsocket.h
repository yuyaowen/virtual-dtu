#ifndef NETSOCKET_H
#define NETSOCKET_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QTcpSocket>
#include <QTcpServer>
#include <QUdpSocket>
#include <QNetworkDatagram>
#include <QAbstractSocket>
#include <QByteArray>
#include <QList>

enum NetProtocol {TCP_SERVER, TCP_CLIENT, UDP};

struct udpRemoteClient {
    QHostAddress ip;
    quint16 port;
};

class NetSocket : public QObject
{
    Q_OBJECT
public:
    NetSocket();
    ~NetSocket();
    void socketStart();
    void socketStop();

    enum NetProtocol protocol = TCP_SERVER;
    QStringList IPList;
    QTcpServer *tcpServerSock = Q_NULLPTR;
    QTcpSocket *curClient = Q_NULLPTR;
    QUdpSocket *udpSock;
    struct udpRemoteClient udpClient;
    QList<QTcpSocket *> clientList;
    QByteArray tcpData;
    bool isHexDisplay = true;
    bool isHexSend = true;
    bool isOnline = false;
    QString ipAddr;
    int port;
    int udpRemotePort;

signals:
    void readDone();
    void socketChanged();

public slots:

private slots:
    void tcpServerNewConnection();
    void tcpServerAccepterr(QAbstractSocket::SocketError socketError);
    void tcpClientErr(QAbstractSocket::SocketError socketError);
    void readData();
};

#endif // NETSOCKET_H
