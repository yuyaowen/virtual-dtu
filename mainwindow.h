#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QByteArray>
#include "serial.h"
#include "netsocket.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void serialReadDone();

    void socketCountUpdate();
    void socketReadDone();

private slots:
    void on_serialSelectComboBox_currentTextChanged(const QString &arg1);
    void on_openSerialPortRadioButton_toggled(bool checked);
    void on_baudRateComboBox_currentTextChanged(const QString &arg1);
    void on_dataBitComboBox_currentTextChanged(const QString &arg1);
    void on_checkBitComboBox_currentIndexChanged(int index);
    void on_stopBitComboBox_currentIndexChanged(int index);
    void on_hexDisplayRadioButton_toggled(bool checked);
    void on_hexSendRadioButton_toggled(bool checked);
    void on_clearRecvPushButton_clicked();
    void on_clearSendPushButton_clicked();
    void on_sendPushButton_clicked();
    void on_recvTextBrowser_textChanged();

    void on_hexDisplayRadioButtonNet_toggled(bool checked);
    void on_recvTextBrowserNet_textChanged();
    void on_clearRecvPushButtonNet_clicked();
    void on_hexSendRadioButtonNet_toggled(bool checked);
    void on_clearSendPushButtonNet_clicked();
    void on_sendPushButtonNet_clicked();
    void on_connectRadioButton_toggled(bool checked);
    void on_protocolSelectComboBox_currentIndexChanged(int index);

private:
    Ui::MainWindow *ui;

    Serial *comPort;
    void updateComPortUI();

    NetSocket *sock;

    QByteArray charToHex(QByteArray chr);
    QByteArray senderToHex(QByteArray chr);
    QByteArray displayCharToHex(QByteArray chr);
    QByteArray displayHexToChar(QByteArray chr);
};

#endif // MAINWINDOW_H
